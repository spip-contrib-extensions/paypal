<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/paypal/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'paypal_description' => 'Pouvoir utiliser Paypal sur SPIP. Peut/pourra aussi fonctionner avec le plugin "abonnement".',
	'paypal_nom' => 'Paypal',
	'paypal_slogan' => 'Utiliser Paypal sur SPIP',

);

?>
