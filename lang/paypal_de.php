<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paypal?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'api_prod' => 'Produktions API',
	'api_test' => 'Test API',

	// C
	'configuration_paypal' => 'Paypal',

	// D
	'description_paypal' => 'Den Onlinebezahldienst Paypal einrichten',

	// E
	'explication_account' => 'Entspricht der E-Mail-Adresse des Verkäuferkontos',
	'explication_environnement' => 'Sie können hier angeben ob sie die Paypal Testwebiste (Sandbox) oder die für gültige Transaktionen nutzen wollen.
Welche Umgebung wollen Sie nutzen?',
	'explication_identifiant_vendeur' => 'Entspricht der E-Mail-Adresse des Verkäuferkontos',
	'explication_soumission' => 'An welche Paypal Seite wird das Formular gesendet?',
	'explication_tax' => 'Eventuell der anzusetzende Mehrwertsteuersatz (in Deutschand 19.00) ',

	// I
	'info_bouton_paypal' => 'Zahlen Sie mit Paypal: es ist schnell, kostenlos und sicher!',

	// L
	'label_account' => 'Konto (E-Mail)',
	'label_currency_code' => 'Währung',
	'label_currency_code_eur' => 'Euro (€)',
	'label_currency_code_usd' => 'Dollar ($)',
	'label_environnement' => 'Umgebung',
	'label_environnement_prod' => 'Produktionsumgebung',
	'label_environnement_test' => 'Testumgebung',
	'label_identifiant_vendeur' => 'Verkäufer ID',
	'label_password' => 'API Passwort (password)',
	'label_signature' => 'Unterschrift ',
	'label_soumission' => 'Ausführung des Formulars',
	'label_tax' => 'Steuern',
	'label_username' => 'API Benutzername '
);
