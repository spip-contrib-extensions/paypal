<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paypal?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'api_prod' => 'Production API',
	'api_test' => 'Test API',

	// C
	'configuration_paypal' => 'Paypal',

	// D
	'description_paypal' => 'Configure the online payment system Paypal',

	// E
	'explication_account' => 'Corresponds to email the seller’s account',
	'explication_environnement' => 'You can indicate in this form datas for the Paypal test website (Sandbox, for development) and the ones for real transaction only.
		Which environment do you want to use ?',
	'explication_identifiant_vendeur' => 'Corresponds to email the seller’s account',
	'explication_soumission' => 'On what page of Paypal is submitted the form?',
	'explication_tax' => 'Eventually the value of the tax to apply (for example, French VAT: 19.60)',

	// I
	'info_bouton_paypal' => 'Make payments with PayPal: it’s fast, free and secure!',

	// L
	'label_account' => 'Account (email)',
	'label_currency_code' => 'Currency',
	'label_currency_code_eur' => 'Euro (€)',
	'label_currency_code_usd' => 'Dollar ($)', # Caractère manquant
	'label_environnement' => 'Environment',
	'label_environnement_prod' => 'Production environment',
	'label_environnement_test' => 'Test environment',
	'label_identifiant_vendeur' => 'Seller ID',
	'label_password' => 'API Password (password)',
	'label_signature' => 'Signature ',
	'label_soumission' => 'Submission Form',
	'label_tax' => 'Taxes',
	'label_username' => 'API Username'
);
