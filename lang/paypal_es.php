<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paypal?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'api_prod' => 'API de producción',
	'api_test' => 'API de prueba',

	// C
	'configuration_paypal' => 'Paypal',

	// D
	'description_paypal' => 'Configurar le sistema de pago  en linea Paypal',

	// E
	'explication_account' => 'Corresponde a la dirección  email de la cuenta del vendedor',
	'explication_environnement' => 'Usted puede indicar en este formulario de datos
		que conciernen el sitio de pruebas de Paypal (Sandbox, para el desarrollo )
		así como las que son válidas solo para verdaderas transacciones.
		Que entorno deséa usted utilizar ?',
	'explication_identifiant_vendeur' => 'Corresponde a la dirección  email de la cuenta del vendedor',
	'explication_soumission' => 'En que página de Paypal es enviado el formulario ?',
	'explication_tax' => 'Eventualmente el valor de la tasa que  debe  aplicarse (ejemplo pour IVA : 21)',

	// I
	'info_bouton_paypal' => 'Efectúe sus pagos con  PayPal :
		una solución rápida, gratuita y segura',

	// L
	'label_account' => 'Cuenta (email)',
	'label_currency_code' => 'Divisa',
	'label_currency_code_eur' => 'Euro (€)',
	'label_currency_code_usd' => 'Dolar ($)',
	'label_environnement' => 'Entorno',
	'label_environnement_prod' => 'Entorno de producción',
	'label_environnement_test' => 'Entorno de prueba',
	'label_identifiant_vendeur' => 'Identificador del vendedor',
	'label_password' => 'API Password (contraseña)',
	'label_signature' => 'Firma',
	'label_soumission' => 'Enviar el formulario',
	'label_tax' => 'Tasas',
	'label_username' => 'API Username (idenficador)'
);
